package com.mono.producer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProducerWorkerApplication

fun main(args: Array<String>) {
	runApplication<ProducerWorkerApplication>(*args)
}
